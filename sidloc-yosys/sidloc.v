/*
 *  SIDLOC FPGA software: SIDLOC beacon software for the ECP5 FPGA
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

`timescale 1 ms/10 us

`include "spi_slave.v"
`include "ctrl.v"



module top (input sys_clk, input spi_clk, input spi_cs, input spi_mosi, output spi_miso, output led, output [1:0] gpios);

  wire spi_read_valid;
  wire [7:0] spi_read;
  wire  spi_write_valid;
  reg [7:0] spi_write;

  spi_slave spi_inst(.i_Rst_L(1), .i_Clk(sys_clk), .o_RX_DV(spi_read_valid), .o_RX_Byte(spi_read),
                     .i_TX_DV(spi_write_valid), .i_TX_Byte(spi_write),
                     .i_SPI_Clk(spi_clk), .o_SPI_MISO(spi_miso), .i_SPI_MOSI(spi_mosi), .i_SPI_CS_n(spi_cs));
  ctrl ctrl_inst(.sys_clk(sys_clk), .rstn(1), .cs_n(spi_cs), .din_valid(spi_read_valid), .din(spi_read), .dout_valid(spi_write_valid),
                 .dout(spi_write), .led_pin(led), .gpio_pins(gpios));

endmodule


