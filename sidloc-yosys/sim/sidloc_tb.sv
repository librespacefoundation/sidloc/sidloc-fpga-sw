/*
 *  SIDLOC FPGA software: SIDLOC beacon software for the ECP5 FPGA
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

`include "../sidloc.v"

module sidloc_tb();


    parameter SYSCLK_PERIOD = 8;
    parameter SPICLK_PERIOD = 200;
    reg sysclk;
    reg spi_clk;
    reg rstn;
    reg cs;
    reg [7:0] mosi_reg;
    reg [2:0] mosi_cnt;
    wire mosi;
    reg miso;
    reg led;
    reg [1:0] gpio;

    reg delay;
    wire sclk;

    assign sclk = spi_clk & delay;
    assign mosi = mosi_reg[mosi_cnt];

    top DUT(.sys_clk(sysclk), .spi_clk(sclk), .spi_cs(cs), .spi_mosi(mosi),
     .spi_miso(miso), .led(led), .gpios(gpio));

    initial begin
        $display("[%0t] Simulation started\n", $time);
        $dumpfile("sidloc_tb.vcd");
        $dumpvars();
        sysclk = 0;
        spi_clk = 0;
        mosi_reg = 0;
        mosi_cnt = 7;
        rstn = 0;
        cs = 1;
        delay = 0;
        #(10*SYSCLK_PERIOD) rstn = 1;

        /* Read transaction at register 0 */

        #(40*SYSCLK_PERIOD) cs = 0;
        #(50) delay = 1;
        #(16*SPICLK_PERIOD)
        #(50)cs = 1;
        #(10*SYSCLK_PERIOD)
        delay = 0;

        /* Write transaction at register 1 */
        mosi_reg = 8'b10000001;
        mosi_cnt = 7;
        #(40*SYSCLK_PERIOD) cs = 0;
        #(50) delay = 1;
        #(16*SPICLK_PERIOD)
        #(50)cs = 1;
        #(10*SYSCLK_PERIOD)
        delay = 0;


        $display("[%0t] Simulation ended\n", $time);
        $finish;
    end

    always #(SYSCLK_PERIOD/2) sysclk = ~sysclk;
    always #(SPICLK_PERIOD/2) spi_clk = ~spi_clk;

    always@(posedge sclk)
    begin
        mosi_cnt <= mosi_cnt - 1;
    end

endmodule
