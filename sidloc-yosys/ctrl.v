/*
 *  SIDLOC FPGA software: SIDLOC beacon software for the ECP5 FPGA
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

`timescale 1 ms/10 us

module ctrl(input sys_clk,
              input rstn,
              input cs_n,
              input din_valid,
              input [7:0] din,
              output reg dout_valid,
              output reg [7:0] dout,
              output led_pin,
              output [1:0] gpio_pins);


  parameter REG_ID   = 0;
  parameter REG_SCRATCH = 1;
  parameter REG_LED = 2;
  parameter REG_GPIO = 3;

  parameter OP_IDLE = 0;
  parameter OP_READ = 1;
  parameter OP_WRITE = 2;


  /* Configuration register space*/
  reg [7:0] id = 8'h37;
  reg [7:0] scratch = 0;
  reg [7:0] led = 8'hFF; //LED is active low
  reg [7:0] gpio = 0;

  reg [1:0] operation = OP_IDLE;
  reg [6:0] reg_addr = 0;
  reg [31:0] byte_cnt = 0;
  reg [31:0] bit_cnt = 0;

  assign led_pin = led[0];
  assign gpio_pins = {sys_clk, dout_valid};

  /* identify the state of the SPI transaction */
  always@(posedge sys_clk)
  begin
    if(cs_n || !rstn)
    begin
      operation <= OP_IDLE;
      reg_addr <= 0;
      byte_cnt <= 0;
    end
    else
    begin
      case (operation)
        OP_IDLE:
        begin
          if(din_valid && din[7] == 0)
          begin
            operation <= OP_READ;
            reg_addr <= din[6:0];
            byte_cnt <= 0;
          end
          else if(din_valid && din[7] == 1)
          begin
            operation <= OP_WRITE;
            reg_addr <= din[6:0];
            byte_cnt <= 0;
          end
          else
          begin
            operation <= OP_IDLE;
            reg_addr <= 0;
          end
        end
        OP_READ:
        begin
          if(din_valid)
          begin
            byte_cnt <= byte_cnt + 1;
          end
        end
        OP_WRITE:
        begin
          if(din_valid)
          begin
            byte_cnt <= byte_cnt + 1;
          end
        end
        default:
        begin
          operation <= OP_IDLE;
          reg_addr <= 0;
          byte_cnt <= 0;
        end
      endcase
    end
  end

  /* Handle SPI transaction */
  always@(posedge sys_clk)
  begin
    if(cs_n || !rstn)
    begin
      dout <= 0;
      dout_valid <= 0;
      bit_cnt <= 0;
    end
    else
    begin
      case (operation)
        OP_IDLE :
        begin
          dout <= 0;
          dout_valid <= 0;
          bit_cnt <= 0;
        end
        OP_READ :
        begin
          if(byte_cnt == 0)
          begin
            if(bit_cnt == 0)
            begin
              dout_valid <= 1;
            end
            else
            begin
              dout_valid <= 0;
            end
            bit_cnt <= bit_cnt + 1;
            case (reg_addr)
              REG_ID :
                dout <= id;
              REG_SCRATCH :
                dout <= scratch;
              REG_LED :
                dout <= ~led;
              REG_GPIO :
                dout <= gpio;
              default:
                dout <= 0;
            endcase
          end
          else
          begin
            dout_valid <= 0;
            dout <= 0;
            bit_cnt <= 0;
          end
        end
        OP_WRITE :
        begin
          case (reg_addr)
            REG_SCRATCH :
            begin
              if(byte_cnt == 1)
              begin
                scratch <= din;
              end
            end
            REG_LED :
              if(byte_cnt == 1)
              begin
                led[0] <= ~din[0];
              end
            REG_GPIO :
              if(byte_cnt == 1)
              begin
                gpio[1:0] <= din[1:0];
              end
          endcase
          bit_cnt <= bit_cnt + 1;
        end
        default:
        begin
          dout <= 0;
          dout_valid <= 0;
          bit_cnt <= 0;
        end
      endcase
    end
  end



endmodule
